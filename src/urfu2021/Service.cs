﻿using System;
namespace urfu2021
{
    public static class Service<T>
    {
        private static T _t;

        public static void RegisterService(T service)
        {
            _t = service;
        }

        public static T GetService()
        {
            return _t;
        }
    }
}
