﻿using System;
using System.Threading.Tasks;

namespace urfu2021
{
    public class FakeHttpService : IHttpService
    {
        public async Task<bool> Auth(string url, string login, string password)
        {
            await Task.Delay(2000);

            if (login == "123")
                return true;
            else
                return false;
        }

        public async Task<string> GetContent()
        {
            await Task.Delay(1000);

            return "My Content";
        }
    }
}
