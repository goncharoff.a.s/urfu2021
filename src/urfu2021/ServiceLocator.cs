﻿using System;
using System.Collections.Generic;

namespace urfu2021
{
    public static class ServiceLocator
    {
        static Dictionary<Type, object> _dict;

        static ServiceLocator()
        {
            _dict = new Dictionary<Type, object>();

            //var a = new A();
            //var type = a.GetType();
            //dict[type] = a;

            //var b = new B();

            //if (b is object)
            //{

            //}
        }

        public static void RegisterService(object service)
        {
            _dict[service.GetType()] = service;
        }

        public static object GetService(Type type)
        {
            return _dict[type];
        }

        //public class A
        //{
        //}

        //public class B : A
        //{
        //}

    }
}
