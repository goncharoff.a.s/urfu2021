﻿using System;

namespace urfu2021
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            RegisterServices(enableFakes: true);

            Start();

            Console.ReadKey();
        }

        public static void RegisterServices(bool enableFakes)
        {
            if (enableFakes)
                Service<IHttpService>.RegisterService(new FakeHttpService());
            else
                Service<IHttpService>.RegisterService(new HttpService());
        }

        public static async void Start()
        {
            var m = new MyLogic();

            m.ContentLoaded += M_ContentLoaded;



            var login = Console.ReadLine();
            var password = Console.ReadLine();
            var content = await m.GetContent(login, password);

            //TODO:
            
        }

        private static void M_ContentLoaded(string content)
        {
            //TODO:
            Console.WriteLine($"My Content:{content}");
        }
    }
}
