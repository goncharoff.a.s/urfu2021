﻿using System;
using System.Threading.Tasks;

namespace urfu2021
{
    public interface IHttpService
    {
        Task<bool> Auth(string url, string login, string password);

        Task<string> GetContent();
    }
}
