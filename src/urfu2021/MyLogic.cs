﻿using System;
using System.Threading.Tasks;

namespace urfu2021
{
    public class MyLogic
    {
        public event Action<string> ContentLoaded;

        public async Task<string> GetContent(string login, string password)
        {
            var _http = Service<IHttpService>.GetService();

            var result = await _http.Auth("url", login, password);

            if (result)
            {
                var content = await _http.GetContent();

                ContentLoaded?.Invoke(content);

                return content;
            }
            else
            {
                return null;
            }
        }
    }
}
