﻿using System;
using System.Collections.Generic;
using UrfuNews21.Domain.DTOs;
using UrfuNews21.ViewModels;
using Xamarin.Forms;

namespace UrfuNews21.Views
{
    public partial class NewsPage : ContentPage, INewsPage
    {
        public NewsPage(NewsItemDTO dto)
        {
            InitializeComponent();
            this.BindingContext = new NewsPageVM(dto);
        }
    }
}
