﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using UrfuNews21.Domain.DTOs;
using UrfuNews21.ViewModels;
using UrfuNews21.Views;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace UrfuNews21
{
    public partial class MainPage : ContentPage, IMainPage
    {
        public MainPage()
        {
            InitializeComponent();

            //this.MyTitle.Text = "Привет!";
            //this.MyButton.Clicked += MyButton_Clicked;

            var vm = new MainPageVM(this);
            this.BindingContext = vm;


            var str = Preferences.Get("news", string.Empty);
            var dto = JsonConvert.DeserializeObject<NewsItemDTO>(str);
        }

        public void ShowNewsPage(NewsItemDTO dto)
        {
            var page = new NewsPage(dto);
            this.Navigation.PushAsync(page);

            var str = JsonConvert.SerializeObject(dto);
            Preferences.Set("news", str);
        }

        //private void ShowNewsPage()
        //{
        //    var page = new NewsPage();
        //    this.Navigation.PushAsync(page);
        //}

        //private void MyButton_Clicked(object sender, EventArgs e)
        //{
        //    var page = new NewsPage();
        //    this.Navigation.PushAsync(page);
        //}
    }
}
