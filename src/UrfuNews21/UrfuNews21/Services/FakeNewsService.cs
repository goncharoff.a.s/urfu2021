﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UrfuNews21.Domain.DTOs;

namespace UrfuNews21.Services
{
    public class FakeNewsService : INewsService
    {
        List<NewsItemDTO> list = new List<NewsItemDTO>();
        public FakeNewsService()
        {
            list = new List<NewsItemDTO>()
            {
                new NewsItemDTO
                {
                    Id = 1,
                    Title = "Новость 1",
                    Desc = "Описание 1ой новости",
                    Data = "23  января 2021 года"
                },

                new NewsItemDTO
                {
                    Id = 2,
                    Title = "Новость 2",
                    Desc = "Описание 2ой новости",
                    Data = "24  января 2021 года"
                },

                new NewsItemDTO
                {
                    Id = 3,
                    Title = "Новость 3",
                    Desc = "Описание 3ой новости",
                    Data = "25  января 2021 года"
                }
            };
        }

        public async Task<List<NewsItemDTO>> LoadListNews()
        {
            await Task.Delay(2000);

            return list;
        }

        public async Task<NewsDTO> LoadNews(int id)
        {
            await Task.Delay(1000);

            var news = list.FirstOrDefault(x => x.Id == id);

            return new NewsDTO
            {
                Id = news.Id,
                Title = news.Title,
                Desc = news.Desc,
                Data = news.Data,
                Content = "Это контент фейковой новости"
            };
        }
    }
}
