﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UrfuNews21.Domain.DTOs;

namespace UrfuNews21.Services
{
    public interface INewsService
    {
        Task<List<NewsItemDTO>> LoadListNews();
        Task<NewsDTO> LoadNews(int id);
    }
}
