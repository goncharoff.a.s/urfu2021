﻿using System;
using System.ComponentModel;
using UrfuNews21.Domain.DTOs;
using UrfuNews21.Services;

namespace UrfuNews21.ViewModels
{
    public class NewsPageVM: INotifyPropertyChanged
    {
        public string _title;
        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                if (value != _title)
                {
                    _title = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Title)));
                }
            }
        }

        public string _content;
        public string Content
        {
            get
            {
                return _content;
            }
            set
            {
                if (value != _content)
                {
                    _content = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Content)));
                }
            }
        }


        NewsItemDTO _dto;

        public event PropertyChangedEventHandler PropertyChanged;

        public NewsPageVM(NewsItemDTO dto)
        {
            _dto = dto;

            LoadNewsByID();
        }

        private async void LoadNewsByID()
        {
            var news = await Service<INewsService>.GetService().LoadNews(_dto.Id);

            Title = news.Title;
            Content = news.Content;

        }
    }
}
