﻿using System;
using UrfuNews21.Domain.DTOs;

namespace UrfuNews21.ViewModels
{
    public interface IMainPage
    {
        void ShowNewsPage(NewsItemDTO item);
    }
}
