﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows.Input;
using UrfuNews21.Domain.DTOs;
using UrfuNews21.Services;
using Xamarin.Forms;

namespace UrfuNews21.ViewModels
{
    public class MainPageVM : INotifyPropertyChanged
    {
        public string _login;
        public string Login
        {
            get
            {
                return _login;
            }
            set
            {
                if (value != _login)
                {
                    _login = value;

                    //if (PropertyChanged != null)
                    //{
                    //    PropertyChanged(this, new PropertyChangedEventArgs(nameof(Login)));
                    //}
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Login)));
                }
            }
        }

        public string _password;
        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                if (value != _password)
                {
                    _password = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Password)));
                }
            }
        }

        public bool _loading;
        public bool Loading
        {
            get
            {
                return _loading;
            }
            set
            {
                if (value != _loading)
                {
                    _loading = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Loading)));
                }
            }
        }

        public ICommand LoginButton { get; set; }

        private List<NewsItemVM> _items;
        public List<NewsItemVM> Items
        {
            get
            {
                return _items;
            }
            set
            {
                if (value != _items)
                {
                    _items = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Items)));
                }
            }
        }

        private NewsItemVM _selectedItem;
        public NewsItemVM SelectedItem
        {
            get
            {
                return _selectedItem;
            }
            set
            {
                if (value != _selectedItem)
                {
                    _selectedItem = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(SelectedItem)));
                    ShowNewsPage(_selectedItem.NewsDTO);
                }
            }
        }

        private void ShowNewsPage(NewsItemDTO item)
        {
            _page.ShowNewsPage(item);
        }

        private IMainPage _page;
        public MainPageVM(IMainPage page)
        {
            _page = page;

            Items = new List<NewsItemVM>();
            LoginButton = new Command(LoginButtonClicked);

            LoadData();
        }

        private async void LoadData()
        {
            //TODO:

            Loading = true;
            var news = await Service<INewsService>.GetService().LoadListNews();

            Items = news.Select(x => new NewsItemVM(x) {Title = x.Title,Desc = x.Desc, Data = x.Data }).ToList();

            
            Loading = false;

            //Items = new List<NewsItemVM>()
            //{
            //    new NewsItemVM()
            //    {
            //        Title = "Новость 1",
            //        Desc = "Описание новости 1",
            //        Data = "23 июля 2021 года"
            //    },
            //     new NewsItemVM()
            //    {
            //        Title = "Новость 2",
            //        Desc = "Описание новости 2",
            //        Data = "23 июля 2021 года"
            //    },
            //     new NewsItemVM()
            //    {
            //        Title = "Новость 3",
            //        Desc = "Описание новости 3",
            //        Data = "23 июля 2021 года"
            //    }
            //};
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void LoginButtonClicked()
        {
            Login = "";
            Password = "";
            _page.ShowNewsPage(null);
        }
    }
}
