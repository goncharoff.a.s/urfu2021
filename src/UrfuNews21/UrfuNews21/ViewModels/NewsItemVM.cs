﻿using System;
using UrfuNews21.Domain.DTOs;

namespace UrfuNews21.ViewModels
{
    public class NewsItemVM
    {
        public NewsItemDTO NewsDTO { get; private set; }

        public NewsItemVM(NewsItemDTO dto)
        {
            NewsDTO = dto;
        }

        public string Title { get; set; }
        public string Desc { get; set; }
        public string Data { get; set; }
    }
}
