﻿using System;
using UrfuNews21;
using UrfuNews21.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace UrfuNews21
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            Service<INewsService>.RegisterService(new FakeNewsService());

            MainPage = new NavigationPage(new MainPage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
