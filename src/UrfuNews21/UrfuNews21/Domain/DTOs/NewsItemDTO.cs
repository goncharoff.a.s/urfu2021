﻿using System;
namespace UrfuNews21.Domain.DTOs
{
    public class NewsItemDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Desc { get; set; }
        public string Data { get; set; }
        public string Author { get; set; }
    }
}
