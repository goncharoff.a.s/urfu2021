﻿using System;
namespace UrfuNews21.Domain.DTOs
{
    public class NewsDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Desc { get; set; }
        public string Data { get; set; }
        public string Author { get; set; }
        public string Url { get; set; }
        public string Content { get; set; }
    }
}
